#!/bin/bash
curl $1 |
	grep '<a href="?sn=' |
	sed -e 's/li/\n/g' |
	grep '<a href="?sn=' |
	sed -e 's/.*<a href="\(.*\)".*/\1/' |
	sed -e 's/\(.*\)/https:\/\/ani.gamer.com.tw\/animeVideo.php\1/'
