# AnigamerDl

A set of scripts for downloading anime from <https://ani.gamer.com.tw/>, based on Puppeteer.

## Dependencies

- Node.js
- Yarn
- Fish

## Usage

- `./dl.fish URL_LIST` downloads the (newline-separated) urls specified in `URL_LIST`.
- `./genlist.sh URL` outputs all availible episodes in a series / season given one episode.

## TODO

- Cookies support for premium accounts.
- Options for download quality. Currently one needs to change the `b2000000` in `dl.js` manually to change the quality.

## License

Unlicense.
