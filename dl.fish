#!/bin/fish
set i 1
for url in (cat "$1");
        set chunk (node dl.js "$url")
        ffmpeg -header 'Origin: https://ani.gamer.com.tw' -v -8 -i "$chunk" -c copy "$i.ts" 2>&1 >/dev/null
        set i (math $i + 1)
end
