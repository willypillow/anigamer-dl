const puppeteer = require('puppeteer');

(async () => {
	const browser = await puppeteer.launch({
		executablePath: '/usr/bin/chromium',
		headless: true,
		args: ['--no-sandbox', '--no-setuid-sandbox']
	});
	const page = await browser.newPage();

	await page.setRequestInterception(true);
	page.on('request', request => {
		if (request.resourceType() === 'xhr') {
			const re = new RegExp('/smil:gamer2\(_fast\)?/.*chunklist.*m3u8');
			if (request.url().match(re)) {
				const repl = new RegExp('chunklist_b.*\.m3u8');
				var url = request.url().replace(repl, 'chunklist_b2000000.m3u8');
				console.log(url);
				browser.close();
				process.exit();
			}
		}
		request.continue();
	});

	await page.goto(process.argv[2]);

	const adultSelector = '#adult';
	await page.waitForSelector(adultSelector);
	await page.click(adultSelector);

	const skipButtonSelector = '//*[contains(text(), \'點此跳過廣告\') or contains(text(), \'Skip ad\')]';
	await page.waitForXPath(skipButtonSelector, {timeout: 40000});
	const skipButton = await page.$x(skipButtonSelector);
	await skipButton[0].click();
})();
